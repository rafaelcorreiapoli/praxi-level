package com.apliant.praxilevel.utils;

import android.widget.EditText;

/**
 * Created by rafa93br on 14/03/2016.
 */
public class Util {
    public static Double safeExtractDouble(EditText editText) {
        return !editText.getText().toString().isEmpty() ? Double.parseDouble(editText.getText().toString()) : 0;
    }
}
