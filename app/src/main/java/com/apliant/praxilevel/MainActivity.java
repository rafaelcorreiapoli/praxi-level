package com.apliant.praxilevel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RelativeLayout;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.apliant.praxilevel.models.Instalacao;
import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "TAG";
    RelativeLayout mCanvas;

    private void launchSetup() {
        Intent i = new Intent(this, SetupActivity.class);
        startActivity(i);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);
            /*
        mCanvas = (RelativeLayout) findViewById(R.id.canvas);
        mCanvas.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.i(TAG, Integer.toString(mCanvas.getHeight()));
                Log.i(TAG, Integer.toString(mCanvas.getWidth()));
                if (Build.VERSION.SDK_INT < 16) {
                    mCanvas.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    mCanvas.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            }
        });
        */

        ActiveAndroid.initialize(this);

        // checar se existe instalacao no banco de dados
        Instalacao instalacao = new Select().from(Instalacao.class).executeSingle();
        if (instalacao != null) {
            Log.i(TAG, "existe instalacao");
        } else {
            Log.i(TAG, "Nao existe instalacao");
            launchSetup();
        }

            /*
        Bomba bombaA = new Bomba(true, 1);
        Bomba bombaB = new Bomba(true, 2);


        Reservatorio res = new ReservatorioCircular(100, 200);
        res.save();
        Reservatorio res2 = new ReservatorioRetangular(10,20,30);
        res2.save();


        //ReservatorioRetangular res3 = new Select().from(ReservatorioRetangular.class).executeSingle();
        //Log.i(TAG, Double.toString(res3.calcularCapacidade()));

        ArrayList<Reservatorio> reservatorios = new ArrayList<Reservatorio>();
        reservatorios.add(res);
        reservatorios.add(res2);

        ConjuntoReservatorios conjuntoSuperior = new ConjuntoReservatorios();
        conjuntoSuperior.setVasoComunicantes(true);
        conjuntoSuperior.save();

        res.setConjuntoReservatorios(conjuntoSuperior);
        res2.setConjuntoReservatorios(conjuntoSuperior);
        res.save();
        res2.save();

        conjuntoSuperior.pushReservatorio(res);
        conjuntoSuperior.pushReservatorio(res2);
        conjuntoSuperior.save();
    */

        /*
        reservatorioS1.save();
        reservatorioS2.save();

        Reservatorio reservatorioI1 = new ReservatorioCircular(10);
        Reservatorio reservatorioI2 = new ReservatorioCircular(10);

        reservatorioI1.save();
        reservatorioI2.save();

        ArrayList<Reservatorio> arr = new ArrayList<Reservatorio>();
        arr.add(reservatorioS1);
        arr.add(reservatorioS2);


        ConjuntoReservatorios conjuntoA = new ConjuntoReservatorios(arr, false);

        ArrayList<Reservatorio> arr2 = new ArrayList<Reservatorio>();
        arr2.add(reservatorioI1);
        arr2.add(reservatorioI2);

        ConjuntoReservatorios conjuntoB = new ConjuntoReservatorios(arr2, true);

        Instalacao i = new Instalacao(bombaA, bombaB, conjuntoA, conjuntoB);

        // checar se existe instalacao no banco de dados
        Instalacao instalacao = new Select().from(Instalacao.class).executeSingle();
        if (instalacao != null) {
            Log.i(TAG, "existe instalacao");
        } else {
            Log.i(TAG, "Nao existe instalacao");
        }

        i.save();
        */
    }
}
