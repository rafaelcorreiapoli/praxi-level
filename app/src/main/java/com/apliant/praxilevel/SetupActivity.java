package com.apliant.praxilevel;

import android.content.Intent;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.apliant.praxilevel.models.Bomba;
import com.apliant.praxilevel.models.Instalacao;
import com.apliant.praxilevel.models.Reservatorio;
import com.apliant.praxilevel.models.ReservatorioCircular;
import com.apliant.praxilevel.models.ReservatorioRetangular;
import com.apliant.praxilevel.singletons.SetupSingleton;
import com.mikepenz.iconics.context.IconicsLayoutInflater;

import java.util.ArrayList;

public class SetupActivity extends BaseActivity implements View.OnClickListener {
    private Button mButtonSetupCisternasSuperiores;
    private Button mButtonSetupCisternasInferiores;
    private Button mButtonSetupBombas;
    private Button mButtonSetupSensores;
    private Button mButtonSetupSalvar;

    SetupSingleton setup;

    private static final String TAG = "TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        LayoutInflaterCompat.setFactory(getLayoutInflater(), new IconicsLayoutInflater(getDelegate()));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);


        mButtonSetupCisternasSuperiores = (Button) findViewById(R.id.buttonSetupCisternasSuperiores);
        mButtonSetupCisternasInferiores = (Button) findViewById(R.id.buttonSetupCisternasInferiores);
        mButtonSetupBombas = (Button) findViewById(R.id.buttonSetupBombas);
        mButtonSetupSensores = (Button) findViewById(R.id.buttonSetupSensores);
        mButtonSetupSalvar = (Button) findViewById(R.id.buttonSetupSalvar);

        mButtonSetupCisternasSuperiores.setOnClickListener(this);
        mButtonSetupCisternasInferiores.setOnClickListener(this);
        mButtonSetupBombas.setOnClickListener(this);
        mButtonSetupSensores.setOnClickListener(this);
        mButtonSetupSalvar.setOnClickListener(this);

        setup = SetupSingleton.getInstance();

        setIcons();



    }

    private void saveReservatorios(ArrayList<Reservatorio> reservatorios) {
        for (Reservatorio reservatorio : reservatorios) {
            if (reservatorio instanceof ReservatorioRetangular) {
                ReservatorioRetangular rRet = (ReservatorioRetangular) reservatorio;
                rRet.save();
            } else if (reservatorio instanceof ReservatorioCircular) {
                ReservatorioCircular rCirc = (ReservatorioCircular) reservatorio;
                rCirc.save();
            }
            reservatorio.save();
        }
    }

    private void saveBombas(ArrayList<Bomba> bombas) {
        for (Bomba bomba : bombas) {
            bomba.save();
        }
    }

    private void concluirConfiguracao() {
        //setup.save();
        Instalacao instalacao = new Instalacao();
        //instalacao.save();

        //setup.getConjuntoBombas().setInstalacao(instalacao);
        setup.getConjuntoBombas().save();
        saveBombas(setup.getConjuntoBombas().getBombas());

        //setup.getConjuntoReservatoriosSuperior().setInstalacao(instalacao);
        setup.getConjuntoReservatoriosSuperior().save();
        saveReservatorios(setup.getConjuntoReservatoriosSuperior().getReservatorios());

        //setup.getConjuntoReservatoriosInferior().setInstalacao(instalacao);
        setup.getConjuntoReservatoriosInferior().save();
        saveReservatorios(setup.getConjuntoReservatoriosInferior().getReservatorios());

        instalacao.setConjuntoBombas(setup.getConjuntoBombas());
        instalacao.setConjuntoReservatoriosSuperior(setup.getConjuntoReservatoriosSuperior());
        instalacao.setConjuntoReservatoriosInferior(setup.getConjuntoReservatoriosInferior());
        instalacao.save();
    }

    private void setIcons() {
        Integer done = R.drawable.ic_check_circle_green_500_48dp;
        Integer needsConfig = R.drawable.ic_settings_black_48dp;

        Boolean bombasOk = setup.getConjuntoBombas() != null;
        Boolean superiorOk = setup.getConjuntoReservatoriosSuperior() != null;
        Boolean inferiorOk = setup.getConjuntoReservatoriosInferior() != null;

        mButtonSetupBombas.setCompoundDrawablesWithIntrinsicBounds(bombasOk ? done : needsConfig, 0, 0, 0);
        mButtonSetupCisternasSuperiores.setCompoundDrawablesWithIntrinsicBounds(superiorOk ? done : needsConfig, 0, 0, 0);
        mButtonSetupCisternasInferiores.setCompoundDrawablesWithIntrinsicBounds(inferiorOk ? done : needsConfig, 0, 0, 0);

        mButtonSetupSalvar.setEnabled(bombasOk && superiorOk && inferiorOk);
    }

    private void launchSetupCisternasInferiores() {
        Intent i = new Intent(this, SetupConjuntoReservatoriosInferioresActivity.class);
        startActivity(i);
    }

    private void launchSetupCisternasSuperiores() {
        Intent i = new Intent(this, SetupConjuntoReservatoriosSuperioresActivity.class);
        startActivity(i);
    }

    private void launchSetupBombas() {
        Intent i = new Intent(this, SetupBombasActivity.class);
        startActivity(i);
    }

    private void launchSetupSensores() {
        Intent i = new Intent(this, SetupCanaisActivity.class);
        startActivity(i);
    }

    private void launchMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.buttonSetupCisternasInferiores:
                launchSetupCisternasInferiores();
                break;
            case R.id.buttonSetupCisternasSuperiores:
                launchSetupCisternasSuperiores();
                break;
            case R.id.buttonSetupBombas:
                launchSetupBombas();
                break;
            case R.id.buttonSetupSensores:
                launchSetupSensores();
                break;
            case R.id.buttonSetupSalvar:
                concluirConfiguracao();
                launchMainActivity();
                break;
        }
    }
}
