package com.apliant.praxilevel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apliant.praxilevel.models.Bomba;
import com.apliant.praxilevel.models.ConjuntoBombas;
import com.apliant.praxilevel.models.ConjuntoReservatorios;
import com.apliant.praxilevel.models.Reservatorio;
import com.apliant.praxilevel.models.ReservatorioRetangular;
import com.apliant.praxilevel.singletons.SetupSingleton;
import com.apliant.praxilevel.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class SetupConjuntoReservatoriosSuperioresActivity extends SetupConjuntoReservatoriosActivity {
    private static final Boolean DEFAULT_VASO_COMUNICANTES = true;
    @Override
    public void save() {
        ConjuntoReservatorios conjuntoReservatorios = new ConjuntoReservatorios();
        conjuntoReservatorios.setVasoComunicantes(mVasoComunicantes);
        conjuntoReservatorios.setAcionamento(Util.safeExtractDouble(mAcionamento));
        conjuntoReservatorios.setDesacionamento(Util.safeExtractDouble(mAcionamento));
        extractReservatorios(conjuntoReservatorios);
        conjuntoReservatorios.setReservatorios(mReservatorios);
        setup.setConjuntoReservatoriosSuperior(conjuntoReservatorios);
        launchSetup();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_cisternas_superiores);


        // Initial setup
        mMaxReservatorios = MAX_RESERVATORIOS_SUPERIORES;
        loadSpinnerOptions();
        loadSetupReservatorios();

        ConjuntoReservatorios cr = setup.getConjuntoReservatoriosSuperior();
        if (cr != null ) {
            mAcionamento.setText(Double.toString(cr.getAcionamento()));
            mVasoComunicantes = cr.getVasoComunicantes();
            mQtdeReservatorios = cr.getReservatorios().size();
            setSetupValues(cr.getReservatorios());
        } else {
            mQtdeReservatorios = DEFAULT_QTDE_RESERVATORIOS_SUPERIORES;
            mVasoComunicantes = DEFAULT_VASO_COMUNICANTES;
        }


        mSpinner.setSelection(mQtdeReservatorios - 1);
        mCheckBoxVasoComunicantes.setChecked(mVasoComunicantes);
    }







}
