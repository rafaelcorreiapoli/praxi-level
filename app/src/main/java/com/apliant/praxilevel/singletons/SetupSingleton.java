package com.apliant.praxilevel.singletons;

import com.apliant.praxilevel.models.Instalacao;

/**
 * Created by rafa93br on 07/03/2016.
 */
public class SetupSingleton extends Instalacao {
    private static SetupSingleton mInstance = null;

    public static SetupSingleton getInstance() {
        if (mInstance == null) {
            mInstance = new SetupSingleton();
        }
        return mInstance;
    }
}
