package com.apliant.praxilevel.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by rafa93br on 02/03/2016.
 */
@Table(name = "Instalacoes")
public class Instalacao extends Model {
    @Column (name = "conjuntoBombas")
    private ConjuntoBombas conjuntoBombas;

    @Column (name = "conjuntoReservatoriosSuperior")
    private ConjuntoReservatorios conjuntoReservatoriosSuperior;

    @Column (name = "conjuntoReservatoriosInferior")
    private ConjuntoReservatorios conjuntoReservatoriosInferior;

    public ConjuntoBombas getConjuntoBombas() {
        return conjuntoBombas;
    }

    public void setConjuntoBombas(ConjuntoBombas conjuntoBombas) {
        this.conjuntoBombas = conjuntoBombas;
    }

    public ConjuntoReservatorios getConjuntoReservatoriosSuperior() {
        return conjuntoReservatoriosSuperior;
    }

    public void setConjuntoReservatoriosSuperior(ConjuntoReservatorios conjuntoReservatoriosSuperior) {
        this.conjuntoReservatoriosSuperior = conjuntoReservatoriosSuperior;
    }

    public ConjuntoReservatorios getConjuntoReservatoriosInferior() {
        return conjuntoReservatoriosInferior;
    }

    public void setConjuntoReservatoriosInferior(ConjuntoReservatorios conjuntoReservatoriosInferior) {
        this.conjuntoReservatoriosInferior = conjuntoReservatoriosInferior;
    }

    public Instalacao(ConjuntoBombas conjuntoBombas, ConjuntoReservatorios conjuntoReservatoriosSuperior, ConjuntoReservatorios conjuntoReservatoriosInferior) {
        this.conjuntoBombas = conjuntoBombas;
        this.conjuntoReservatoriosSuperior = conjuntoReservatoriosSuperior;
        this.conjuntoReservatoriosInferior = conjuntoReservatoriosInferior;
    }

    public Instalacao() {

    }

}
