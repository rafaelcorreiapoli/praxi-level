package com.apliant.praxilevel.models;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by rafa93br on 02/03/2016.
 */
@Table(name="ReservatoriosRetangulares")
public class ReservatorioRetangular extends Reservatorio{
    @Column(name="largura")
    double largura;
    @Column(name="altura")
    double altura;

    public double getLargura() {
        return largura;
    }

    public void setLargura(float largura) {
        this.largura = largura;
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    @Override
    public double calcularCapacidade() {
        return largura * altura * profundidade;
    }

    public ReservatorioRetangular(double largura, double altura, double profundidade) {
        super(profundidade);
        this.largura = largura;
        this.altura = altura;
    }

    public ReservatorioRetangular() {

    }
}
