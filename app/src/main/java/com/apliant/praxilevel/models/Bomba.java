package com.apliant.praxilevel.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by rafa93br on 02/03/2016.
 */
@Table(name = "Bombas")
public class Bomba extends Model {
    @Column(name = "ligada")
    private boolean ligada;
    @Column(name = "vazao")
    private double vazao;
    @Column(name = "conjuntoBombas")
    private ConjuntoBombas conjuntoBombas;

    public boolean isLigada() {
        return ligada;
    }

    public void setLigada(boolean ligada) {
        this.ligada = ligada;
    }

    public double getVazao() {
        return vazao;
    }

    public void setVazao(double vazao) {
        this.vazao = vazao;
    }

    public Bomba() {
        super();
    }

    public Bomba(boolean ligada, double vazao) {
        super();
        this.ligada = ligada;
        this.vazao = vazao;
    }

    public ConjuntoBombas getConjuntoBombas() {
        return conjuntoBombas;
    }

    public void setConjuntoBombas(ConjuntoBombas conjuntoBombas) {
        this.conjuntoBombas = conjuntoBombas;
    }
}
