package com.apliant.praxilevel.models;

/**
 * Created by rafa93br on 02/03/2016.
 */

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name="ReservatoriosCirculares")
public class ReservatorioCircular extends Reservatorio {
    @Column(name="raio")
    private double raio;

    public double getRaio() {
        return raio;
    }

    public void setRaio(float raio) {
        this.raio = raio;
    }

    @Override
    public double calcularCapacidade() {
        return Math.pow(raio, 2) * Math.PI;
    }

    public ReservatorioCircular(double raio, double profundidade) {
        super(profundidade);
        this.raio = raio;
    }

    public ReservatorioCircular() {}

}
