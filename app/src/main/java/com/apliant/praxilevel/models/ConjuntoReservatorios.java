package com.apliant.praxilevel.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rafa93br on 02/03/2016.
 */
@Table(name = "ConjuntosReservatorios")
public class ConjuntoReservatorios extends Model {
    ArrayList<Reservatorio> reservatorios = new ArrayList<Reservatorio>();

    @Column(name = "vasoComunicantes")
    Boolean vasoComunicantes;

    @Column(name = "acionamento")
    private Double acionamento;
    @Column(name = "desacionamento")
    private Double desacionamento;


    public Boolean getVasoComunicantes() {
        return vasoComunicantes;
    }

    public void setVasoComunicantes(Boolean vasoComunicantes) {
        this.vasoComunicantes = vasoComunicantes;
    }

    public void pushReservatorio(Reservatorio reservatorio) {
        reservatorios.add(reservatorio);
    }
    public void pullReservatorio(int index) {
        reservatorios.remove(index);
    }

    public ConjuntoReservatorios(ArrayList<Reservatorio> reservatorios, Boolean vasoComunicantes, Double acionamento) {
        this.reservatorios = reservatorios;
        this.vasoComunicantes = vasoComunicantes;
        //  histerese
        this.acionamento = acionamento;
        this.desacionamento = acionamento;
    }

    public ConjuntoReservatorios(ArrayList<Reservatorio> reservatorios, Boolean vasoComunicantes, Double acionamento, Double desacionamento) {
        this.reservatorios = reservatorios;
        this.vasoComunicantes = vasoComunicantes;
        this.acionamento = acionamento;
        this.desacionamento = desacionamento;
    }

    public ArrayList<Reservatorio> getReservatorios() {
        return reservatorios;
    }

    public void setReservatorios(ArrayList<Reservatorio> reservatorios) {
        this.reservatorios = reservatorios;
    }

    public ConjuntoReservatorios() {}

    public Double getAcionamento() {
        return acionamento;
    }

    public void setAcionamento(Double acionamento) {
        this.acionamento = acionamento;
    }

    public Double getDesacionamento() {
        return desacionamento;
    }

    public void setDesacionamento(Double desacionamento) {
        this.desacionamento = desacionamento;
    }

}
