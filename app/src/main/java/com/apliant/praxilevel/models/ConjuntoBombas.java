package com.apliant.praxilevel.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;

/**
 * Created by rafa93br on 07/03/2016.
 */
@Table(name = "ConjuntoBombas")
public class ConjuntoBombas extends Model{
    @Column(name = "dependentes")
    private Boolean dependentes;
    ArrayList<Bomba> bombas = new ArrayList<Bomba>();

    public Boolean getDependentes() {
        return dependentes;
    }

    public void setDependentes(Boolean dependentes) {
        this.dependentes = dependentes;
    }

    public ArrayList<Bomba> getBombas() {
        return bombas;
    }

    public void setBombas(ArrayList<Bomba> bombas) {
        this.bombas = bombas;
    }

    public ConjuntoBombas(Boolean dependentes, ArrayList<Bomba> bombas) {
        this.dependentes = dependentes;
        this.bombas = bombas;
    }

    public void pushBomba(Bomba bomba) {
        bombas.add(bomba);
    }
    public void pullBomba(int index) {
        bombas.remove(index);
    }

    public ConjuntoBombas() {}

}
