package com.apliant.praxilevel.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by rafa93br on 02/03/2016.
 */
@Table(name="Reservatorios")
public class Reservatorio extends Model {
    @Column(name="profundidade")
    protected double profundidade;

    @Column(name="conjuntoReservatorios")
    protected ConjuntoReservatorios conjuntoReservatorios;

    public double calcularCapacidade() {
        return 0;
    };

    public Reservatorio(double profundidade) {
        this.profundidade = profundidade;
    }

    public Reservatorio() {
    }

    public double getProfundidade() {
        return profundidade;
    }

    public void setProfundidade(double profundidade) {
        this.profundidade = profundidade;
    }

    public ConjuntoReservatorios getConjuntoReservatorios() {
        return conjuntoReservatorios;
    }

    public void setConjuntoReservatorios(ConjuntoReservatorios conjuntoReservatorios) {
        this.conjuntoReservatorios = conjuntoReservatorios;
    }
}
