package com.apliant.praxilevel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apliant.praxilevel.models.Bomba;
import com.apliant.praxilevel.models.ConjuntoReservatorios;
import com.apliant.praxilevel.models.Reservatorio;
import com.apliant.praxilevel.models.ReservatorioRetangular;
import com.apliant.praxilevel.singletons.SetupSingleton;

import java.util.ArrayList;
import java.util.List;

public abstract class SetupConjuntoReservatoriosActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener{
    protected static final Integer MAX_RESERVATORIOS_SUPERIORES = 10;
    protected static final Integer MAX_RESERVATORIOS_INFERIORES = 10;
    protected static final Integer DEFAULT_QTDE_RESERVATORIOS_SUPERIORES = 2;
    protected static final Integer DEFAULT_QTDE_RESERVATORIOS_INFERIORES = 2;
    private static final String TAG = "TAG";

    protected Spinner mSpinner;
    protected Button mSalvar;
    protected LinearLayout mLinearLayout;
    protected CheckBox mCheckBoxVasoComunicantes;
    protected ArrayList<View> setupReservatorios = new ArrayList<View>();
    protected ArrayList<Reservatorio> mReservatorios = new ArrayList<Reservatorio>();
    protected Boolean mVasoComunicantes = false;
    protected Integer mQtdeReservatorios;
    protected Integer mMaxReservatorios;

    protected EditText mAcionamento;

    SetupSingleton setup = SetupSingleton.getInstance();


    protected void launchSetup() {
        Intent i = new Intent(this, SetupActivity.class);
        startActivity(i);
    }

    public abstract void save();

    protected void extractReservatorios(ConjuntoReservatorios conjuntoReservatorios) {
        mReservatorios.clear();
        for (int i = 0; i < mQtdeReservatorios; i++) {
            View v = setupReservatorios.get(i);
            EditText editTextProfundidade = (EditText) v.findViewById(R.id.editTextProfundidade);
            EditText editTextLargura = (EditText) v.findViewById(R.id.editTextLargura);
            EditText editTextAltura = (EditText) v.findViewById(R.id.editTextAltura);

            Double profundidade = 0.0;
            Double largura = 0.0;
            Double altura = 0.0;
            try {
                profundidade = Double.parseDouble(editTextProfundidade.getText().toString());
                largura = Double.parseDouble(editTextLargura.getText().toString());
                altura = Double.parseDouble(editTextAltura.getText().toString());
            } catch(NumberFormatException e) {

            }


            Reservatorio reservatorio = new ReservatorioRetangular(largura, altura, profundidade);
            reservatorio.setConjuntoReservatorios(conjuntoReservatorios);
            mReservatorios.add(reservatorio);
        }
    }

    protected void loadSetupReservatorios() {
        for (int i = 0; i < mMaxReservatorios; i++) {
            // View to setup reservatorio
            View setupReservatorio = LayoutInflater.from(this).inflate(R.layout.setup_reservatorio, null);
            TextView titulo = (TextView) setupReservatorio.findViewById(R.id.textViewTitulo);
            titulo.setText("Reservatório " + (i + 1));
            setupReservatorios.add(setupReservatorio);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 20);
            mLinearLayout.addView(setupReservatorio, layoutParams);
        }
    }

    protected void setSetupValues(ArrayList<Reservatorio> reservatorios) {
        for (int i = 0; i < reservatorios.size(); i++) {
            Reservatorio res = reservatorios.get(i);
            View v = setupReservatorios.get(i);

            EditText profundidade = (EditText) v.findViewById(R.id.editTextProfundidade);
            profundidade.setText(Double.toString(res.getProfundidade()));

            if (res instanceof ReservatorioRetangular) {
                ReservatorioRetangular resRet = (ReservatorioRetangular) res;

                EditText largura = (EditText) v.findViewById(R.id.editTextLargura);
                EditText altura = (EditText) v.findViewById(R.id.editTextAltura);

                largura.setText(Double.toString(resRet.getLargura()));
                altura.setText(Double.toString(resRet.getAltura()));
            }
        }
    }

    protected void showHideSetupReservatorios() {
        for (int i = 0; i < mMaxReservatorios; i++) {
            View setupBomba = setupReservatorios.get(i);
            setupBomba.setVisibility(i < mQtdeReservatorios ? View.VISIBLE : View.GONE);
        }
    }

    protected void loadSpinnerOptions() {
        List<Integer> quantities = new ArrayList<Integer>();
        for (int i = 1; i <= mMaxReservatorios; i++) {
            quantities.add(i);
        }
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, quantities);
        mSpinner.setAdapter(dataAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_setup_conjunto_reservatorios);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        // UI
        mSpinner = (Spinner) findViewById(R.id.spinner_qtde_reservatorios);
        mLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_setup_reservatorios);
        mSalvar = (Button) findViewById(R.id.buttonSalvarConfigReservatorioSup);
        mSpinner.setOnItemSelectedListener(this);
        mCheckBoxVasoComunicantes = (CheckBox) findViewById(R.id.checkBoxVasoComunicantes);

        mAcionamento = (EditText) findViewById(R.id.editTextAcionamento);

        // Listeners
        mSalvar.setOnClickListener(this);
        mCheckBoxVasoComunicantes.setOnCheckedChangeListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSalvarConfigReservatorioSup:
                save();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mQtdeReservatorios = position + 1;
        showHideSetupReservatorios();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.checkBoxVasoComunicantes:
                mVasoComunicantes = isChecked;
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
