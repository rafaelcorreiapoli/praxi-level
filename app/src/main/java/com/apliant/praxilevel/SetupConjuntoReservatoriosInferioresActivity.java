package com.apliant.praxilevel;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.apliant.praxilevel.models.ConjuntoReservatorios;
import com.apliant.praxilevel.utils.Util;

public class SetupConjuntoReservatoriosInferioresActivity extends SetupConjuntoReservatoriosActivity {
    private static final Boolean DEFAULT_VASO_COMUNICANTES = true;
    private Toolbar mToolbar;
    private EditText mDesacionamento;

    @Override
    public void save() {
        ConjuntoReservatorios conjuntoReservatorios = new ConjuntoReservatorios();
        conjuntoReservatorios.setVasoComunicantes(mVasoComunicantes);
        conjuntoReservatorios.setAcionamento(Util.safeExtractDouble(mAcionamento));
        conjuntoReservatorios.setDesacionamento(Util.safeExtractDouble(mDesacionamento));
        extractReservatorios(conjuntoReservatorios);
        conjuntoReservatorios.setReservatorios(mReservatorios);
        setup.setConjuntoReservatoriosInferior(conjuntoReservatorios);
        launchSetup();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_cisternas_inferiores);

        mDesacionamento = (EditText) findViewById(R.id.editTextDesacionamento);

        // Initial setup
        mMaxReservatorios = MAX_RESERVATORIOS_INFERIORES;
        loadSpinnerOptions();
        loadSetupReservatorios();

        ConjuntoReservatorios cr = setup.getConjuntoReservatoriosInferior();
        if (cr != null ) {
            mDesacionamento.setText(Double.toString(cr.getAcionamento()));
            mAcionamento.setText(Double.toString(cr.getAcionamento()));
            mVasoComunicantes = cr.getVasoComunicantes();
            mQtdeReservatorios = cr.getReservatorios().size();
            setSetupValues(cr.getReservatorios());
        } else {
            mQtdeReservatorios = DEFAULT_QTDE_RESERVATORIOS_INFERIORES;
            mVasoComunicantes = DEFAULT_VASO_COMUNICANTES;
        }


        mSpinner.setSelection(mQtdeReservatorios - 1);
        mCheckBoxVasoComunicantes.setChecked(mVasoComunicantes);
    }
}
