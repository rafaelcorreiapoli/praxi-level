package com.apliant.praxilevel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.apliant.praxilevel.models.Bomba;
import com.apliant.praxilevel.models.ConjuntoBombas;
import com.apliant.praxilevel.singletons.SetupSingleton;
import com.apliant.praxilevel.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class SetupBombasActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, Button.OnClickListener{
    private static final Integer MAX_BOMBAS = 10;
    private static final Integer DEFAULT_QTDE_BOMBAS = 2;
    private static final Boolean DEFAULT_DEPENDENTES = true;

    private static final String TAG = "TAG";
    private Toolbar mToolbar;

    private RadioGroup mRadioModoOperacao;
    private Button mButtonSalvarConfig;
    private LinearLayout mLinearLayout;
    private Spinner mSpinner;

    private Integer qtdeBombas;
    private ArrayList<View> setupBombasArray = new ArrayList<View>();
    private ArrayList<View> radioQtdeBombasArray = new ArrayList<View>();

    private ArrayList<Bomba> mBombas = new ArrayList<Bomba>();
    private Boolean mDependentes;

    SetupSingleton setup;

    //
    //  Load Spinner Options (1,2,3..)
    //
    private void loadSpinnerOptions() {
        List<Integer> quantities = new ArrayList<Integer>();
        for (int i = 1; i <= 10; i++) {
            quantities.add(i);
        }
        ArrayAdapter<Integer> dataAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, quantities);
        mSpinner.setAdapter(dataAdapter);
    }

    //
    //  Load Views to setup Bombas
    //
    private void loadSetupBombasView() {
        for (int i = 0; i < MAX_BOMBAS; i++) {
            // View to setup bomb
            View setupBomba = LayoutInflater.from(this).inflate(R.layout.setup_bomba, null);
            TextView titulo = (TextView) setupBomba.findViewById(R.id.textViewTitulo);
            titulo.setText("Bomba " + (i + 1));
            setupBombasArray.add(setupBomba);
            mLinearLayout.addView(setupBomba, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    private void setSetupValues(ArrayList<Bomba> bombas) {
        for (int i = 0; i < bombas.size(); i++) {
            Bomba bomba = bombas.get(i);
            View v = setupBombasArray.get(i);
            EditText vazao = (EditText) v.findViewById(R.id.editTextVazao);
            vazao.setText(Double.toString(bomba.getVazao()));
        }
    }

    //
    //  Show only necessary Bombas
    //
    private void showHideSetupBombas() {
        for (int i = 0; i < MAX_BOMBAS; i++) {
            View setupBomba = setupBombasArray.get(i);
            setupBomba.setVisibility(i < qtdeBombas ? View.VISIBLE : View.GONE);
        }
    }

    //
    //  On Create
    //
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup_bombas);

        setup = SetupSingleton.getInstance();

        // Toolbar
        mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // UI
        mSpinner = (Spinner) findViewById(R.id.spinner_qtde_bombas);
        loadSpinnerOptions();
        mLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_bombas);
        mButtonSalvarConfig = (Button) findViewById(R.id.buttonSalvarConfigBombas);
        mRadioModoOperacao = (RadioGroup) findViewById(R.id.radioModoOperacao);



        // Listeners
        mRadioModoOperacao.setOnCheckedChangeListener(this);
        mButtonSalvarConfig.setOnClickListener(this);
        mSpinner.setOnItemSelectedListener(this);

        // Initial setup
        loadSetupBombasView();

        // Check existing setupSingleton
        ConjuntoBombas cj = setup.getConjuntoBombas();
        if (cj != null) {
            mDependentes = cj.getDependentes();
            qtdeBombas = cj.getBombas().size();
            setSetupValues(cj.getBombas());
        } else {
            mDependentes = DEFAULT_DEPENDENTES;
            qtdeBombas = DEFAULT_QTDE_BOMBAS;
        }

        mSpinner.setSelection(qtdeBombas - 1);
        mRadioModoOperacao.check(mDependentes ? R.id.radioDependentes : R.id.radioIndependentes);

    }



    //
    //  Read all views and create Bombas instances
    //
    private void extractBombas(ConjuntoBombas conjuntoBomba) {
        mBombas.clear();
        for (int i = 0; i < qtdeBombas; i++) {
            View v = setupBombasArray.get(i);
            EditText editTextVazao = (EditText) v.findViewById(R.id.editTextVazao);
            Double mVazao = Util.safeExtractDouble(editTextVazao);
            Bomba bomba = new Bomba(false, mVazao);
            bomba.setConjuntoBombas(conjuntoBomba);
            mBombas.add(bomba);
        }
    }

    //
    //  Save method
    //
    private void save() {
        ConjuntoBombas conjuntoBombas = new ConjuntoBombas();
        conjuntoBombas.setDependentes(mDependentes);
        extractBombas(conjuntoBombas);
        conjuntoBombas.setBombas(mBombas);
        setup.getInstance().setConjuntoBombas(conjuntoBombas);
        launchSetup();
    }

    private void launchSetup() {
        Intent i = new Intent(this, SetupActivity.class);
        startActivity(i);
    }

    //
    //  Handle button click
    //
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSalvarConfigBombas:
                save();
                break;
        }
    }

    //
    //  Handle Radio Group
    //
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        Log.i(TAG, "on check changed listener " + checkedId);
        switch (group.getId()) {
            case R.id.radioModoOperacao:
                mDependentes = checkedId == R.id.radioDependentes;
                break;
        }

        Log.i(TAG, Boolean.toString(mDependentes));


    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        qtdeBombas = position + 1;
        showHideSetupBombas();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
